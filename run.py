#!/usr/bin/env python3
# coding: utf8

"""
Author: freezed <git@freezed.me> 2019-12-10
Licence: `GNU AGPL v3` GNU AGPL v3: http://www.gnu.org/licenses/

This file is part of [TaskLab](https://gitlab.com/free_zed/tasklab)
"""

from flask import abort, Flask, render_template
import gitlab
import os

app = Flask(__name__)
gl = gitlab.Gitlab(os.environ["GITLAB_URL"], private_token=os.environ["PRIVATE_TOKEN"])


@app.before_request
def before_request():
    """ GitLab authentication """
    try:
        gl.auth()
    except Exception as except_detail:
        abort(403)


@app.route("/")
def index():
    return render_template("index.j2", context=gl.user.name)


@app.route("/my/")
def current_user():
    return render_template("gl_object.j2", context=gl.user)


@app.route("/my/todos")
def todo():
    todos = [t.target["title"] for t in gl.todos.list(state="pending", type="Issue")]
    return render_template("gl_list.j2", context=todos)


@app.route("/pendings")
def pending():
    pendings = [p.title for p in gl.issues.list(state="opened") if p.assignees == []]
    return render_template("gl_list.j2", context=pendings)


@app.route("/my/projects/")
def projects():
    projects = [(p.id, p.name) for p in gl.projects.list(all=True)]
    return render_template("gl_list.j2", context=projects)


@app.route("/my/projects/<int:project_id>/")
def project_details(project_id):
    return render_template("gl_object.j2", context=gl.projects.get(project_id))


@app.route("/my/projects/<int:project_id>/issues/")
def project_issues(project_id):
    issues = [
        (i.id, i.title) for i in gl.projects.get(project_id).issues.list(state="opened")
    ]
    return render_template("gl_list.j2", context=issues)


@app.route("/my/projects/<int:project_id>/board")
def project_issue_board(project_id):
    project = gl.projects.get(project_id)
    board = project.boards.list()[0]
    board_lists = [b_list for b_list in board.lists.list()]
    project_board = {}

    for b_list in board_lists:
        project_board.update(
            {
                b_list.label["name"]: [
                    (issue.iid, issue.title)
                    for issue in project.issues.list(state="opened")
                    if b_list.label["name"] in issue.labels
                ]
            }
        )

    return render_template("board.j2", context=project_board)
