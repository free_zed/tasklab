TaskLab
=======

A basic task management tool.

In fact a light client for [GitLab API][glapi] using :

* [`python-gitlab`][pygl]
* [`flask`][flask]

Try it !
--------

1. Set a virtual environment : `virtualenv .venv && source .venv/bin/activate`
1. Install dependencies : `pip install -r requirements.txt`
1. Go on your _GitLab_ instance and [set a new API token][gli]
1. Set environment variables :
    - `export GITLAB_URL="https://gitlab.com"`
    - `export PRIVATE_TOKEN="thislooklikeabadtoken"`
    - `export FLASK_ENV=development`
    - `export FLASK_APP=run.py`
1. Run Flask : `flask run`
1. Say hello : `curl http://127.0.0.1:5000/`

---

Alternatively, you can set vars inside your _virtualenv_ (`.venv`):

Add at bottom of `deactivate()` func° in  `.venv/bin/activate`

```shell
    unset FLASK_APP
    unset FLASK_ENV
    unset GITLAB_URL
    unset PRIVATE_TOKEN
```

Add at bottom of `.venv/bin/activate`:

```shell
    export FLASK_APP=run.py
    export FLASK_ENV=development
    export GITLAB_URL="https://gitlab.com"
    export PRIVATE_TOKEN="thislooklikeabadtoken"
```

[glapi]:    https://docs.gitlab.com/ce/api/
[pygl]:     https://pypi.org/project/python-gitlab/
[flask]:    https://palletsprojects.com/p/flask/
[gli]:      https://gitlab.com/profile/personal_access_tokens
